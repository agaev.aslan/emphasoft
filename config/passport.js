const passport = require('passport');
const VKontakteStrategy = require('passport-vkontakte').Strategy;
const models = require('../models');

passport.serializeUser((user, done) => {
  done(null, { _id: user.id });
});

passport.deserializeUser(async (sessionData, done) => {
  let user;
  try {
    user = await models.UserModel.findOne({ where: { id: sessionData._id } });
    if (!user) {
      done(null, false);
    }
      done(null, user);
  } catch (error) {
    console.log(error)
    done(null, false);
  }
});

passport.use(new VKontakteStrategy({
    clientID: process.env.VK_CLIENT_ID,
    clientSecret: process.env.VK_CLIENT_SECRET,
    callbackURL: process.env.VK_CALLBACK_URL,
    profileFields: ['email'],
    // profileFields: ['email', 'bdate', 'verified'],
  }, async (accessToken, refreshToken, params, profile, done) => {
    const userData = {
      email: params.email,
      firstName: profile.name.givenName,
      lastName: profile.name.familyName,
      username: profile.username,
      vkontakteId: profile.id,
      accessToken,
      refreshToken,
    };
    let user = await models.UserModel.findOne({ where: { vkontakteId: userData.vkontakteId } });
    if (!user) {
      user = await models.UserModel.create(userData);
    }  
    try {
      done(null, user);        
    } catch (error) {
        console.log(error);
    }
  }));
  