import axios from 'axios';

async function getProfile() {
    try {
        return axios.get(`https://emphasoft.herokuapp.com/user`).then((user) => {
            return user.data;
        });
    } catch (error) {
        console.log(error)
    }
}

export const userService = {
    getProfile,
};