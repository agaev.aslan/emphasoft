import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import VkLogin from './login/VkLogin'

class Body extends Component {
    render() {
      return (
        <div>
          <Switch>
            <Route exact path='/' component={VkLogin} />
          </Switch>
      </div>
    )
  }
}

export default Body;
