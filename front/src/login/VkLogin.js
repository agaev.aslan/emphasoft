import React from 'react';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faVk } from "@fortawesome/free-brands-svg-icons"
import './bootstrap-social.css';

import { userService } from '../services';


const apiUrl = process.env.NODE_ENV !== 'production' ? `http://localhost:5000` : `https://emphasoft.herokuapp.com`;

class VkLogin extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            firstName: '',
            lastName: '',
            friends: [],
            loading: false,
        };
    }

    componentWillMount() {
        this.setState({ loading: true })
        userService.getProfile().then((user) => {
            this.setState({
                firstName: user.firstName,
                lastName: user.lastName,
                friends: user.friends,
                loading: false,
            })
        })
        .catch((e) => {
            this.setState({ loading: false })
        })
    }

    render() {
        if (this.state.loading) {
            return (
                <div> Loading...</div>
            );
        } else if (this.state.friends.length > 0) {
            this.listItems = this.state.friends.map((friend) => {
                return <li>  { friend.first_name } { friend.last_name } </li>;
            });
            return (
                <div className="container">
                    <div className="row d-flex justify-content-center" style={{ paddingTop: '15%' }}>
                        <div className="col-xs-7 col-sm-7 col-sm-7 col-lg-7 col-xl-7" >
                            <p> {this.state.firstName} {this.state.lastName} </p>
                            <p> Friends: </p>
                            <ul>{this.listItems}</ul>
                            <a href={`${apiUrl}/user/logout`}>Logout</a>
                        </div>
                    </div>
                </div>
            )
        } else {
            return (
                <div className="container">
                    <div className="row d-flex justify-content-center" style={{ paddingTop: '15%' }}>
                        <div className="col-xs-7 col-sm-7 col-sm-7 col-lg-7 col-xl-7" >
                            <a href={`${apiUrl}/auth/vk`} className="btn btn-social btn-vk text-white btn-md btn-block">
                                <div>
                                    <FontAwesomeIcon icon={faVk} />
                                </div>
                                Sign in with Vkontakte
                            </a>
                        </div>
                    </div>
                </div>
            )
        }
        
    }
}

export default VkLogin;