const express = require('express');
const passport = require('passport');
const axios = require('axios');
const models = require('../models')

const router = express.Router();

router.get('/auth/vk', passport.authenticate('vkontakte', { scope: ['email'] }));

router.get('/auth/vk/callback',
  passport.authenticate('vkontakte', { failureRedirect: '/' }),
  (req, res) => {
    if (req.user) {
      res.redirect(`${process.env.URL}`);
      return req.user;
    }
  }
);

router.get('/user', async (req, res) => {
  try {
    if (typeof req.user === 'undefined') {
      res.status(401).send();
      return;
    } 
    const user = req.user.dataValues;
    const friendsList = await axios.get(`https://api.vk.com/method/friends.get?order=random&count=5&fields=nickname&access_token=${user.accessToken}&v=5.103`)
    let userData = {
        friends: friendsList.data.response.items,
        firstName: user.firstName,
        lastName: user.lastName,
    }
    res.send(userData);
  } catch (e) {
    res.status(500).send();
  }
});

router.get('/user/logout', async (req, res) => {
  try {
    res.redirect(`${process.env.URL}`);
    req.session.destroy();
  } catch (error) {
    res.status(400).send();
  }
}); 

module.exports = router;
