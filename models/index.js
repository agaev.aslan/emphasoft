const Sequelize = require('sequelize');

const connectionString = process.env.DATABASE_URL;

const sequelize = new Sequelize(connectionString, {
    logging: false,
    dialect: 'postgres',
    protocol: 'postgres',
    dialectOptions: {
      ssl: true
    },
    pool: {
      max: 5,
      min: 0,
      idle: 20000,
      acquire: 200000,
    },
});

sequelize.sync().then(() => {
  console.log('Database Synced');
});

const UserModel = require('./user')(sequelize, Sequelize);

const models = {
    UserModel,
}

models.sequelize = sequelize;
models.Sequelize = Sequelize;

module.exports = models;
