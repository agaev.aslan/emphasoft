require('dotenv').config()
const express = require('express');
const path = require('path');
const session = require('express-session');
const redis = require('redis');
const RedisStore = require('connect-redis')(session);
const cookieParser = require('cookie-parser');
const passport = require('passport');
const cors = require('cors');
require('./config/passport');

const apiRoutes = require('./routes/api');

const app = express();

let allowedOrigins = [];
if (process.env.ALLOWED_ORIGINS) {
  allowedOrigins = process.env.ALLOWED_ORIGINS.split(',');
}
const corsOptions = {
  origin: (origin, callback) => {
    if (!origin) return callback(null, true);
    if (allowedOrigins.indexOf(origin) === -1) {
      const msg = 'The CORS policy for this site does not '
        + 'allow access from the specified Origin.';
      return callback(new Error(msg), false);
    }
    return callback(null, true);
  },
  credentials: true,
};

app.options('*', cors(corsOptions));
app.use(cors(corsOptions));

const client = redis.createClient(process.env.REDIS_URL);
const sessionStore = new RedisStore({ client });
const sessionMiddleware = session({
  store: sessionStore,
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: false,
  cookie: { secure: false },
});


app.use(sessionMiddleware);
app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(apiRoutes);

app.use(express.static(path.join(__dirname, 'front/build')));

app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname+'/front/build/index.html'));
});

const port = process.env.PORT || 5000;
app.listen(port);

console.log(`App listening on ${port}`);